from __future__ import division
import numpy as np
import scipy.io as sio
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math


def reduce_data(x):
    """
    reduce dataset to prepare it for reshape
    :param x: dataset to be reduced
    :return: reduced dataset
    """
    # Pick some random values and removed them
    # FUTURE TASK
    remainder = np.size(x) % 21
    new_array = x[:-remainder]
    return new_array


def split_dataset(x):
    """
    Split dataset into training, validation and test data
    :param x: input dataset
    :return: train_data, validation_data, test_dat
    """
    border_train = int(0.7 * np.shape(x)[0])
    border_valid = int(0.85 * np.shape(x)[0])
    train_data = x[0:border_train]
    valid_data = x[border_train:border_valid]
    test_data = x[border_valid:]
    return train_data, valid_data, test_data

def split_subsets(x):
    """
    Split last column of given dataset
    :param x: input dataset
    :return: X dataset, y dataset
        with X containing all but the last column of input x
        and y being a column vector just containing the last column of x
    """
    return  x[:, :-1], x[:, -1]


def create_vander_times(start, end, order=1):
    """
    Creates Vandermonde matrix up until given order and a times vector from
    start to end in 1/20 steps
    :param start: start point = first included time
    :param end: end point = first time NOT to be included
    :param order: order of polynomial

    :return: vandermonde_matrix, times (column vector)

    general idea of Vandermonde Matrix:

        | 1  start          start^2          start^3         ...  start^order         |
        | 1 (start + 1/20) (start + 1/20)^2 (start + 1/20)^3 ... (start + 1/20)^order |
        | 1 (start + 2/20) (start + 2/20)^2 (start + 2/20)^3 ... (start + 2/20)^order |
        |                                   ...                                       |
        | 1 (end - 1/20)   (end - 1/20)^2   (end - 1/20)^3   ... (end - 1/20)^order   |

    shape: ((end - start) * 20, order + 1)

    times (column vector) just represents the 2nd column of X_time_matrix
    """
    times = np.arange(start, end - 0.01, 0.05)
    vandermonde_matrix = np.vander(times, order+1, True)
    return vandermonde_matrix, times


# Ex. 3b i)
def Phi(C, K):
    assert C <= 20, "C has to be smaller than 20 you stupid Martin"
    start_point = 1 - C/20
    return create_vander_times(start_point, 1, K-1)[0]


# Ex. 3b ii)
def make_vv(C, K):
    phi = Phi(C, K)
    phi_one = np.ones((K, 1))
    # Invert is expensive, is there any other way ?
    return np.dot(phi, np.dot(np.linalg.inv(np.dot(np.transpose(phi), phi)), phi_one))


# used random seeds:
random_seeds = [0, 23, 1, 867]
np.random.seed(0)

# Question 1 a)
# loading of data
path = "amp_data.mat"
amp_data = sio.loadmat(path)["amp_data"]
print(amp_data.shape)

# plot line graph showing entire dataset sequence
plt.figure()
plt.title("amp_data sequence")
plt.plot(amp_data)

# plot histogram graph of the amplitudes in this dataset
plt.figure()
plt.title("amp_data amplitude histogram")
plt.hist(amp_data, bins=100)


# preparing data
new_data = reduce_data(amp_data)
new_data = np.reshape(new_data, (int(np.size(new_data)/21), 21))
# shuffling data
np.random.shuffle(new_data)
# splitting data into training, validation and test-set with
# respective y value (21st value)
train_data, valid_data, test_data = split_dataset(new_data)
X_shuf_train, y_shuf_train = split_subsets(train_data)
# /).-
y_shuf_train = np.expand_dims(y_shuf_train, axis=1)
X_shuf_val, y_shuf_val = split_subsets(valid_data)
y_shuf_val = np.expand_dims(y_shuf_val, axis=1)
X_shuf_test, y_shuf_test = split_subsets(test_data)
y_shuf_test = np.expand_dims(y_shuf_test, axis=1)


# Question 2 a)
# linear fit
X, times= create_vander_times(0, 1)
x_row = X_shuf_train[0,:]
y_true = y_shuf_train[0]

# fit linear regression model to x_row
w = np.linalg.lstsq(X, x_row, rcond=None)[0]

y_pred = np.dot(X, w)
y_pred_21 = np.dot(np.array([1 ,1]), w)

# plot fitted model + true x_row values
plt.figure()
plt.title("Linear Regression fits")
plt.scatter(times, x_row, marker='x', label="training_data")
plt.scatter(1, y_true, color='orange', label="y_true")
plt.plot([0, 1], [y_pred[0], y_pred_21], color='r', label='linear regression')

# polynomial fit
order = 4
X_poly, times = create_vander_times(0, 1, order)

# fit polynomial linear regression to x_row
w_poly = np.linalg.lstsq(X_poly, x_row, rcond=None)[0]

y_pred_poly = np.dot(X_poly, w_poly)
y_pred_21_poly = np.dot(np.array([1 for i in range(order + 1)]), w_poly)

times_until_one = [i/20 for i in range(21)]
# plot polynomial model
plt.plot(times_until_one, np.insert(y_pred_poly, np.shape(y_pred_poly)[0], y_pred_21_poly), label='polynomial (order 4) linear regression', c="g")
plt.legend()


# Question 3 b) iii)
# simulates linear model prediction
v_lin = make_vv(20, 2)
v_lin_pred = np.dot(v_lin.T, x_row)
print(v_lin_pred[0], y_pred_21)
assert abs(v_lin_pred[0] - y_pred_21) < 0.001, "Prediction of typical linear regression model and v computed model varies!"

# simulates polynomial model prediction
v_poly = make_vv(20, 5)
v_poly_pred = np.dot(v_poly.T, x_row)
print(v_poly_pred[0], y_pred_21_poly)
assert abs(v_poly_pred[0] - y_pred_21_poly) < 0.001, "Prediction of polynomial linear regression model and v computed model varies!"


# Question 3 c) i)
best_K = -1
best_C = -1
best_mse = None
best_v = None
x_values = []
y_values = []
z_values = []

for C in range(1, 20):
   for K in range(2, 10):
       # print(C, K)
       v = make_vv(C, K)
       pred = np.dot(X_shuf_train[:, -C:], v)
       mse = np.mean(np.square(y_shuf_train - pred))
       if mse < 0.0001:
           x_values.append(C)
           y_values.append(K)
           z_values.append(mse)
       if (best_mse is None or best_mse > mse):
           print("new best found")
           best_K = K
           best_C = C
           best_mse = mse
           best_v = v
print("Q3 c) i) Results:")
print("Best Training MSE of %f was reached with:" % best_mse)
print("K = %d" % best_K)
print("C = %d" % best_C)

plt.figure()
ax = plt.gca(projection='3d')
ax.scatter(x_values, y_values, z_values)
ax.set_xlabel("C")
ax.set_ylabel("K")
ax.set_zlabel("MSE")
plt.title("C/ K MSE values")

# Question 3 c) ii)
# validation set MSE
pred_val = np.dot(X_shuf_val[:, -best_C:], best_v)
mse_val = np.mean(np.square(y_shuf_val - pred_val))
print("Validation set MSE: %.8f" % mse_val)
# test set MSE
pred_test = np.dot(X_shuf_test[:, -best_C:], best_v)
mse_test = np.mean(np.square(y_shuf_test - pred_test))
print("Test set MSE: %.8f" % mse_test)

# Question 4 a)
n_rows = X_shuf_train.shape[0]
best_C_train = -1
best_mse_train = None
best_C_val = -1
best_mse_val = None
best_v_val = None
for C in range(1, 21):
    v, sse, _, _ = np.linalg.lstsq(X_shuf_train[:n_rows,-C:], y_shuf_train[:n_rows], rcond=None)
    mse = sse / n_rows
    if best_mse_train is None or best_mse_train > mse:
        best_C_train = C
        best_mse_train = mse
    pred_val = np.dot(X_shuf_val[:, -C:], v)
    mse_val = np.mean(np.square(y_shuf_val - pred_val))
    if best_mse_val is None or best_mse_val > mse_val:
        best_mse_val = mse_val
        best_C_val = C
        best_v_val = v
print(best_C_train)
print(best_mse_train)
print(best_C_val)
print(best_mse_val)

# Question 4 b)
# Question 3 model
C_q3 = 2
pred_q3_test = np.dot(X_shuf_test[:, -C_q3:], best_v)
mse_q3_test = np.mean(np.square(y_shuf_test - pred_q3_test))
print(mse_q3_test)

# Question 4 val model -> best
C_q4 = 16
pred_q4_test = np.dot(X_shuf_test[:, -C_q4:], best_v_val)
mse_q4_test = np.mean(np.square(y_shuf_test - pred_q4_test))
print(mse_q4_test)

pred_q4_val = np.dot(X_shuf_val[:, -C_q4:], best_v_val)
res_q4_val = (y_shuf_val - pred_q4_val)

plt.figure()
plt.hist(res_q4_val, bins=100)
plt.title("")
plt.show()
