# Linear Regression - Predicting Audio

Raw audio data is represented as a sequence of amplitudes. Lossless audio compression systems (like flac) use a model to predict each amplitude in turn, given the
sequence so far. The residuals of the predictions are compressed and stored, from which the audio file can be reconstructed. We intially used **vanilla linear regression**, and then 
enchanced it with **basis functions of polynomial predictors of different order**. We also used **varying sequence history** to identify the best length of history. 

### Build With 

* Python
* Numpy 
* Matplotlib
* PyCharm 